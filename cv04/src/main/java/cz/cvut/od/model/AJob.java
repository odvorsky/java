package cz.cvut.od.model;

import java.util.Objects;

public class AJob extends AbstractJob {

    private String name = "A Job";

    public String getName() {
        return name;
    }

    protected void processInternal() {
        System.out.println("JOB A IS EXECUTING");
    }

    @Override
    public String toString() {
        return "AJob{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AJob aJob = (AJob) o;
        return Objects.equals(name, aJob.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
