package cz.cvut.od.model;

public interface Job {

    void process();

    String getName();
}
