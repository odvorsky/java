package cz.cvut.od.model;

public class BJob extends AbstractJob {

    public String getName() {
        return "Job B";
    }

    protected void processInternal() {
        System.out.println("JOB B IS EXECUTING");
    }
}
