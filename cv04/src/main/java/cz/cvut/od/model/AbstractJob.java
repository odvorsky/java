package cz.cvut.od.model;

public abstract class AbstractJob implements Job{

    protected  abstract void processInternal();

    public void process(){
        System.out.println("JOB START");
        System.out.println(getName());
        processInternal();
        System.out.println("JOB FINISHED");
    }

}
