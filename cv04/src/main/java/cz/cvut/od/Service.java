package cz.cvut.od;

import cz.cvut.od.model.*;

public class Service {

    public void start() {
        //example1();
        //example2();
        //example3();
        example4();
    }

    public void example4(){
        for (int i = 0; i < 5; i++) {
            perfTest();
        }
    }

    public void perfTest(){
        long start = System.currentTimeMillis();

        long end = System.currentTimeMillis();
        System.out.println(end - start);

    }

    public void example3(){
        BirthdayPresent<String> present = new BirthdayPresent<>();
        present.setContent("AAAA");

        System.out.println(present.getContent());

        BirthdayPresent<Integer> present2 = new BirthdayPresent<>();
        present2.setContent(11);

        System.out.println(present2.getContent() + 2);


    }

    public void example1() {
        Job a = new AJob();
        Job b = new BJob();

        a.process();
        b.process();
    }

    public void example2() {
        int[] arr = new int[10];
        for (int i = 0; i < 10; i++) {
            arr[i] = i + 1;
        }
        int[] arr2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i = 0; i < arr2.length; i++) {
            int value = arr2[i];
            if(value % 2 == 0){
                System.out.println(value);
            }
        }
        Job[] jobArr = new Job[3];
        jobArr[0] = new AJob();
        jobArr[1] = new BJob();
        jobArr[2] = new CJob();
        for (int i = 0; i < jobArr.length; i++) {
            jobArr[i].process();
        }


    }


}
