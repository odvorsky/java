package cz.cvut.od;

import cz.cvut.od.model.*;
import cz.cvut.od.service.UserDeletionService;

public class Main {

    public static void main(String[] args){

        Admin.info();

        System.out.println(Calculator.sum((float) 10, 10));
        System.out.println(Calculator.sum(10,10,10));

        Polygon square = new Square(5);
        System.out.println(square.getArea());

        Animal cat = new Cat();
        cat.info();
        cat.makeSound();

        Animal dog = new Dog();
        dog.info();
        dog.makeSound();

        Animal duck = new Duck();
        duck.info();
        duck.makeSound();

        User basicUser = new User("user1", "pw");
        User basicUser2 = new User("user1", "pw");

        User adminUser = new Admin("admin1", "pw");

        if(adminUser instanceof Admin){
            System.out.println("adminUser is Admin");
        }

        UserDeletionService service = new UserDeletionService();
        service.deleteUser(basicUser);
        service.deleteUser(adminUser);

        System.out.println(basicUser);

        if(basicUser.equals(basicUser2)){
            System.out.println("EQUALS");
        }

    }

}
