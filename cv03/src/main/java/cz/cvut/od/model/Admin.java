package cz.cvut.od.model;

public class Admin extends User {

    private String privileges;

    public Admin(String username, String password) {
        super(username, password);
    }

    public void addItemToCatalog(String item){
        System.out.println("ITEM: " + item + "ADDED TO CATALOG");
    }

    public String getPrivileges() {
        return privileges;
    }

    public void setPrivileges(String privileges) {
        this.privileges = privileges;
    }

    public static void info(){
        System.out.println("ADMIN STATIC INFO INVOKED");
    }

    @Override
    public boolean canBeDeleted() {
        return false;
    }
}
