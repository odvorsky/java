package cz.cvut.od.model;

public interface Deletable {

    boolean canBeDeleted();
}
