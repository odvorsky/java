package cz.cvut.od.model;

public abstract class Animal {

    public void info(){
        System.out.println("ANIMAL");
    }

    public abstract void makeSound();

}
