package cz.cvut.od.model;

public class Square implements Polygon{
    private int a;

    public Square(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getArea() {
        return a*a;
    }
}
