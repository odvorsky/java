package cz.cvut.od.model;

import java.util.Date;
import java.util.Objects;

public class User implements Deletable{

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Date lastLoggedDate;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void login(){
        System.out.println("USER LOGGED IN");
    }

    public void logout(){
        System.out.println("USER LOGGED OUT");
    }

    public void addToCart(String item){
        System.out.println("ITEM: " + item + " ADDED TO CART");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static void info(){
        System.out.println("USER STATIC INFO INVOKED");
    }

    public Date getLastLoggedDate() {
        return lastLoggedDate;
    }

    public void setLastLoggedDate(Date lastLoggedDate) {
        this.lastLoggedDate = lastLoggedDate;
    }

    public boolean canBeDeleted() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", lastLoggedDate=" + lastLoggedDate +
                '}';
    }
}
