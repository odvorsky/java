package cz.cvut.od.model;

public class SubAdmin extends User {

    public SubAdmin(String username, String password) {
        super(username, password);
    }

    @Override
    public boolean canBeDeleted() {
        return false;
    }
}
