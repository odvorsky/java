package cz.cvut.od.model;

public class Calculator {


    public static float sum(int a, float b) {
        return a + b;
    }

    public static float sum(float a, int b) {
        return a + b;
    }

    public static int sum(int a, int b, int c) {
        return a + b + c;
    }
}
