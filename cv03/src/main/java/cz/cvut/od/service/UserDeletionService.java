package cz.cvut.od.service;

import cz.cvut.od.model.User;

public class UserDeletionService {

    public void deleteUser(User user){
        if(user.canBeDeleted()){
            System.out.println("user: " + user.getUsername() + " was deleted");
        }
    }
}
