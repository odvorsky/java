package cz.cvut.od.model;

public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public synchronized void greetBack(User friend){
        System.out.println(this.name + " greets back " + friend.getName());
    }

    public synchronized void greet(User friend){
        System.out.println(this.name + " greets " + friend.getName());
        friend.greetBack(this);
    }

    public String getName() {
        return name;
    }
}
