package cz.cvut.od.model;

public class Polling {

    private boolean condition;


    public synchronized void notifyCondition(){
        System.out.println("NASTAVUJI PODMINKU");
        this.condition = true;
        this.notify();
    }

    public synchronized void waitCondition(){
        while(!this.condition){
            try {
                System.out.println("CEKAM NA PODMINKU");
                this.wait();
                System.out.println("PODMINKA SPLNENA");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public synchronized boolean isCondition() {
        return condition;
    }

    public synchronized void setCondition(boolean condition) {
        this.condition = condition;
    }
}
