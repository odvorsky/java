package cz.cvut.od.model;

public class Count {

    private int count1;
    private int count2;

    private final Object lock = new Object();
    private final Object lock2 = new Object();

    public void incCount1(){
        synchronized (lock){
            System.out.println("count1: " + count1);
            count1++;
        }
    }

    public void incCount2(){
        synchronized (lock2){
            System.out.println("count2: " + count2);
            count2++;
        }
    }

    public synchronized int getCount1() {
        return count1;
    }

    public synchronized int getCount2() {
        return count2;
    }
}
