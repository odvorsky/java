package cz.cvut.od;

import cz.cvut.od.service.ConcurrentPlayground;

public class Main {


    public static void main(String[] args){
        new ConcurrentPlayground().play();
    }
}
