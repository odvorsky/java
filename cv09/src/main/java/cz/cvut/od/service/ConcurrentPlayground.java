package cz.cvut.od.service;

import cz.cvut.od.model.Count;
import cz.cvut.od.model.Polling;
import cz.cvut.od.model.User;

import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentPlayground {

    private final ReentrantLock lock = new ReentrantLock();

    public void play(){
        this.polling();
    }

    public void polling() {
        final Polling polling = new Polling();

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("CEKAM NA NASTAVENI PODMINKY");
                    Thread.sleep(3000);
                    polling.notifyCondition();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                polling.waitCondition();
            }
        });
        t1.start();
        t2.start();
    }

    public void deadlock(){
        final User user1 = new User("david");
        final User user2 = new User("michal");

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                user1.greet(user2);
            }
        });
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                user2.greet(user1);
            }
        });

        t1.start();
        t2.start();

    }

    public void syncBlockIndependent(){
        final Count count = new Count();

        Thread t1 = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while(i < 100){
                    count.incCount1();
                    i++;
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while(i < 100){
                    count.incCount1();
                    i++;
                }
            }
        });

        Thread t3 = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while(i < 100){
                    count.incCount2();
                    i++;
                }
            }
        });

        Thread t4 = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while(i < 100){
                    count.incCount2();
                    i++;
                }
            }
        });

        t1.start();
        t3.start();
        t2.start();
        t4.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(count.getCount1());
        System.out.println(count.getCount2());

    }

}
