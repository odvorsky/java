package cz.cvut.od;

import cz.cvut.od.model.Game;
import cz.cvut.od.model.User;

import java.util.*;

public class Main {

    public static final void main(String[] args){

        Set<String> set = new HashSet<>(Arrays.asList("A", "B", "A", "C"));
        for (String item: set) {
            System.out.println(item);
        }

        Set<User> set2 = new HashSet<>(
                Arrays.asList(new User("Aleš", ""), new User("Aleš", ""), new User("David", "")));
        for (User item: set2) {
            System.out.println(item);
        }

        List<String> stringList = new ArrayList<>();
        stringList.add("A");
        stringList.add("B");
        stringList.add("C");

        Iterator<String> iterator1 = stringList.iterator();
        while(iterator1.hasNext()){
            String next = iterator1.next();
            if(next.equals("A")){
                iterator1.remove();
            }
        }
        System.out.println(stringList);

        Iterator<User> iterator = set2.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println("===============");
        User ales = new User("Aleš", "");
        Map<String, User> map = new HashMap<>();
        map.put("aleš", ales);
        map.put("ivan", ales);
        map.put("aleš", ales);

        for (String username: map.keySet()) {
            User user = map.get(username);
            System.out.println(user);
        }

        for (Map.Entry<String, User> entry :map.entrySet()) {
            System.out.println(entry.getValue());
        }

        User one = new User("X", "David");
        Game game = new Game(one, one, 4);
        //game.turn(1,1);
        //game.turn(1,2);


    }

}
