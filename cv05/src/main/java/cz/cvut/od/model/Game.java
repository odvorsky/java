package cz.cvut.od.model;

import java.time.LocalDateTime;

public class Game {

    private User one;
    private User two;

    private Desk desk;
    private User playerOnTurn;
    private int numberOfTurns;
    private LocalDateTime gameStartTime;

    public Game(User one, User two, int deskSize) {
        this.one = one;
        this.two = two;
        this.playerOnTurn = this.one;
        this.desk = new Desk(deskSize);
        this.gameStartTime = LocalDateTime.now();
    }

    public Desk getDesk() {
        return desk;
    }

    public void setDesk(Desk desk) {
        this.desk = desk;
    }

    public User getOne() {
        return one;
    }

    public void setOne(User one) {
        this.one = one;
    }

    public User getTwo() {
        return two;
    }

    public void setTwo(User two) {
        this.two = two;
    }

    public User getPlayerOnTurn() {
        return playerOnTurn;
    }

    public void setPlayerOnTurn(User playerOnTurn) {
        this.playerOnTurn = playerOnTurn;
    }

    public int getNumberOfTurns() {
        return numberOfTurns;
    }

    public void setNumberOfTurns(int numberOfTurns) {
        this.numberOfTurns = numberOfTurns;
    }

    public LocalDateTime getGameStartTime() {
        return gameStartTime;
    }

    public void setGameStartTime(LocalDateTime gameStartTime) {
        this.gameStartTime = gameStartTime;
    }
}
