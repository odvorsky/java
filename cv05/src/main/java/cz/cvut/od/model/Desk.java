package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Desk {

    private static final int MAX_DESK_SIZE = 6;
    private int deskSize;
    private List<List<DeskField>> desk;

    public Desk(int deskSize){
        //TODO kontrola ze nemame vic nez deskSize throw new IllegalArgumentException("zadana hodnota je moc velka");
        this.deskSize = deskSize;
        this.desk = new ArrayList<>(deskSize);
        //TODO vytvorit deskfieldy a sub listy
    }

    public List<List<DeskField>> getDesk() {
        return desk;
    }

    public void setDesk(List<List<DeskField>> desk) {
        this.desk = desk;
    }
}


