package cz.cvut.od.model;

public class DeskField {

    private boolean checked;
    private User owner;

    public void check(User owner){
        this.checked = true;
        this.owner = owner;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
