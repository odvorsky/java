package cz.cvut.od.cv12.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TicTacServiceMock implements TicTacService {

    private static final Logger log = LoggerFactory.getLogger(TicTacServiceImpl.class);

    @Override
    public void createNewGame() {
        log.info("MOCK SERVICE CALLED");
    }
}
