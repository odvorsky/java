package cz.cvut.od.cv12.controller;

import cz.cvut.od.cv12.model.User;
import cz.cvut.od.cv12.repository.UserRepository;
import cz.cvut.od.cv12.service.TicTacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tictac")
public class TicTacController {

    private TicTacService ticTacService;
    private UserRepository userRepository;

    @Autowired
    public TicTacController(TicTacService ticTacService, UserRepository userRepository){
        this.ticTacService = ticTacService;
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/hello/{name}")
    public User hello(@PathVariable String name, @RequestParam(value = "username", required = false) String username){
        ticTacService.createNewGame();
        return new User(name, username);
    }

    @GetMapping(value = "/goodbye")
    public String goodBye(){
        ticTacService.createNewGame();
        return "NASHLE";
    }

    @PostMapping(value = "/user")
    public User saveUser(@RequestBody User user){
        return userRepository.save(user);
    }

    @GetMapping(value = "/user/{id}")
    public User getUser(@PathVariable Long id){
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException("not found"));
    }

    @GetMapping(value = "/user/username/{username}")
    public List<User> getUsers(@PathVariable String username){
        return userRepository.findByUsername(username);
    }

}
