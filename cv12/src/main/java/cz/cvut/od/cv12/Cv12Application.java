package cz.cvut.od.cv12;

import cz.cvut.od.cv12.service.TicTacService;
import cz.cvut.od.cv12.service.TicTacServiceMock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class Cv12Application {

	@Bean
	@Profile("mock")
	public TicTacService ticTacServiceMock(){
		return new TicTacServiceMock();
	}

	public static void main(String[] args) {
		SpringApplication.run(Cv12Application.class, args);
	}

}
