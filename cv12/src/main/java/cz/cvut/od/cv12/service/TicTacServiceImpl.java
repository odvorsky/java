package cz.cvut.od.cv12.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("impl")
public class TicTacServiceImpl implements TicTacService {

    private static final Logger log = LoggerFactory.getLogger(TicTacServiceImpl.class);

    @Override
    public void createNewGame() {
        log.info("CALLED CREATE NEW GAME");
    }
}
