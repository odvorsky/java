package cz.cvut.od;

import cz.cvut.od.ui.CommandLineUi;

public class Main {

    public static void main(String[] args){
        new CommandLineUi().start();
    }
}
