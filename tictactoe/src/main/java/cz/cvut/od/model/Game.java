package cz.cvut.od.model;

import cz.cvut.od.model.exception.DeskInputException;
import cz.cvut.od.model.exception.DeskSizeInvalidException;

import java.time.LocalDateTime;

public class Game {

    private User one;
    private User two;

    private Desk desk;
    private User playerOnTurn;
    private int numberOfTurns;
    private int turnsToWin;
    private User winner;
    private LocalDateTime gameStartTime;

    public Game(User one, User two, int deskSize, int turnsToWin) throws DeskSizeInvalidException {
        this.one = one;
        this.two = two;
        this.playerOnTurn = this.one;
        this.turnsToWin = turnsToWin;
        this.desk = new Desk(deskSize);
        this.gameStartTime = LocalDateTime.now();
    }

    public void turn(int x, int y) throws DeskInputException {
        this.desk.turn(x, y, playerOnTurn);

        if(playerOnTurn.equals(this.one)){
            playerOnTurn = this.two;
        }
        else {
            playerOnTurn = this.one;
        }
        this.winner = this.evaluateWinner();
    }

    public User evaluateWinner(){
        //TODO GET DESK AND TURNS TO WIN - returns winning User if present
        return null;
    }

    public Desk getDesk() {
        return desk;
    }

    public void setDesk(Desk desk) {
        this.desk = desk;
    }

    public User getOne() {
        return one;
    }

    public void setOne(User one) {
        this.one = one;
    }

    public User getTwo() {
        return two;
    }

    public void setTwo(User two) {
        this.two = two;
    }

    public User getPlayerOnTurn() {
        return playerOnTurn;
    }

    public void setPlayerOnTurn(User playerOnTurn) {
        this.playerOnTurn = playerOnTurn;
    }

    public int getNumberOfTurns() {
        return numberOfTurns;
    }

    public void setNumberOfTurns(int numberOfTurns) {
        this.numberOfTurns = numberOfTurns;
    }

    public LocalDateTime getGameStartTime() {
        return gameStartTime;
    }

    public void setGameStartTime(LocalDateTime gameStartTime) {
        this.gameStartTime = gameStartTime;
    }

    public int getTurnsToWin() {
        return turnsToWin;
    }

    public void setTurnsToWin(int turnsToWin) {
        this.turnsToWin = turnsToWin;
    }

    public User getWinner() {
        return winner;
    }
}
