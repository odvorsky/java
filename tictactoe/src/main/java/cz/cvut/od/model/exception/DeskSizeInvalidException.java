package cz.cvut.od.model.exception;

public class DeskSizeInvalidException extends Exception {

    public DeskSizeInvalidException() {
    }

    public DeskSizeInvalidException(String message) {
        super(message);
    }
}
