package cz.cvut.od.net;

import cz.cvut.od.model.User;

import java.io.*;
import java.net.Socket;
import java.util.Objects;

public class UserThread extends Thread {

    private Socket socket;

    private GameQueue gameQueue;
    private ServerGame serverGame;
    private String name;

    private BufferedReader input;
    private PrintWriter output;
    private User user = new User();


    public UserThread(Socket socket, GameQueue gameQueue) {
        super("UserThread");
        this.socket = socket;
        this.gameQueue = gameQueue;
        this.name = this.getName();
    }

    @Override
    public void run() {
        System.out.println("NEW USER THREAD STARTED..");
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            output.println("SERVER_READY:Server připraven k příjmu komunikace");
            String line;
            do {
                line = this.readLine();
                System.out.println(this.getName() + ": " + line);
                if (line.contains(":")) {
                    String[] split = line.split(":");
                    String cmd = split[0];
                    String data = split[1];
                    if ("USERNAME".equals(cmd)) {
                        this.user.setUsername(data);
                    }
                    if ("MARK".equals(cmd)) {
                        this.user.setMark(data);
                    }
                }
                if (user.getUsername() == null) {
                    output.println("REQ_USERNAME:Poskytněte uživatelské jméno");
                }
                if (user.getUsername() != null && user.getMark() == null) {
                    output.println("REQ_MARK:Poskytněte uživatelskou značku");
                }
                if (user.isComplete()) {
                    output.println("WAITING_GAME:Vyčkejte na připojení dalšího hráče");
                    gameQueue.play(this);
                    output.println("GAME_PAIRED:Hra napárována");
                }
            } while (!"EXIT".equals(line));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeQuietly(input, output, socket);
        }
    }

    private String readLine() {
        try {
            return input.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void closeQuietly(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            try {
                if (closeable != null) {
                    closeable.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserThread that = (UserThread) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public User getUser() {
        return user;
    }

    public ServerGame getServerGame() {
        return serverGame;
    }

    public void setServerGame(ServerGame serverGame) {
        this.serverGame = serverGame;
    }
}
