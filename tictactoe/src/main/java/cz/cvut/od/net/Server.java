package cz.cvut.od.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {

    private static final Logger log = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) throws IOException {
        GameQueue gameQueue = new GameQueue();
        ServerSocket serverSocket = new ServerSocket(4444);
        log.info("SERVER STARTED AND LISTENING...");
        while (true) {
            new UserThread(serverSocket.accept(), gameQueue).start();
        }
    }
}
