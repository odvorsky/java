package cz.cvut.od.net;

import cz.cvut.od.model.Game;
import cz.cvut.od.model.exception.DeskSizeInvalidException;

public class ServerGame {

    private UserThread userOne;
    private UserThread userTwo;
    private Game game;

    public ServerGame(UserThread userOne, UserThread userTwo){
        try {
            this.game = new Game(userOne.getUser(), userTwo.getUser(), 6, 4);
            userOne.setServerGame(this);
            userTwo.setServerGame(this);
        } catch (DeskSizeInvalidException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }


}
