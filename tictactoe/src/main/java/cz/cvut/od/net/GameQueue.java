package cz.cvut.od.net;

import java.util.ArrayDeque;
import java.util.Deque;

public class GameQueue {

    private final Deque<UserThread> queue = new ArrayDeque<>();

    public void play(UserThread userOne) {
        synchronized (this.queue) {
            if (this.queue.size() > 0) {
                UserThread userTwo = this.queue.removeFirst();
                new ServerGame(userOne, userTwo);
                this.queue.notifyAll();
            } else {
                this.queue.add(userOne);
                try {
                    while(queue.contains(userOne)){
                        this.queue.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
