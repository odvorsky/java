package cz.cvut.od;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        new IOPlayground().playground();
    }
}
