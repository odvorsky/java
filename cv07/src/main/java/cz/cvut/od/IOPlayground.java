package cz.cvut.od;

import java.io.*;
import java.util.Scanner;

public class IOPlayground {

    public void playground() throws IOException {
        //basicSystemIn();
        //inputStreamReader();
        //bufferedReader();
        //scanner();
        //console();

        //fileRead();
        fileWrite();
    }

    public void fileWrite() throws IOException {
        try(BufferedWriter fileWriter = new BufferedWriter(new FileWriter("/Users/odvorsky/Desktop/JAVA/git/java/cv07/src/main/resources/filewrite.txt"))){
            fileWriter.write("AAAAAAA");

        }

    }

    public void fileRead() throws IOException {
        BufferedReader fileReader = null;
        try {
            fileReader = new BufferedReader(new FileReader("/Users/odvorsky/Desktop/JAVA/git/java/cv07/src/main/resources/file.txt"));
            String line;
            do {
                line = fileReader.readLine();
                if (line != null) {
                    System.out.println(line);
                }
            }
            while (line != null);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(fileReader != null){
                fileReader.close();
            }
        }
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("/Users/odvorsky/Desktop/JAVA/git/java/cv07/src/main/resources/file.txt"))){
            String line;
            do {
                line = bufferedReader.readLine();
                if (line != null) {
                    System.out.println(line);
                }
            }
            while (line != null);
        }


    }

    public void basicSystemIn() throws IOException {
        while(true){
            System.out.println((char) System.in.read());
        }
    }

    public void inputStreamReader() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        while(true){
            System.out.println((char) inputStreamReader.read());
        }
    }

    public void bufferedReader() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
            System.out.println(bufferedReader.readLine());
        }
    }

    public void scanner() {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            System.out.println(scanner.nextLine());
        }
    }

    public void console(){
        System.console().readLine();
    }

}
