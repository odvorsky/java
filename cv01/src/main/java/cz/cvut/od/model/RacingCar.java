package cz.cvut.od.model;

public class RacingCar extends Car {

    private int topSpeed;

    public RacingCar() {

        System.out.println("RacingCar constructor called");

    }
    public RacingCar(Engine engine, int topSpeed) {
        super(engine);
        this.topSpeed = topSpeed;
    }

    public RacingCar(String brand, String color, String year, Engine engine) {
        super(brand, color, year, engine);
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }
}
