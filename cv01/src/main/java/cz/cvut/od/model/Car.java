package cz.cvut.od.model;

public class Car {

    public static int COUNT = 1;

    private String brand;
    private String color;
    private String year;

    private Engine engine;


    public Car() {
        System.out.println("Car constructor called");
    }

    public Car(Engine engine){
        this.engine = engine;
    }

    public Car(String brand, String color, String year, Engine engine) {
        this(engine);
        this.brand = brand;
        this.color = color;
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Engine getEngine() {
        return engine;
    }

    public String getText(){
        return "Car: počet:" + COUNT + ", " + this.color + ", " + this.brand + ", " + this.year + ", motor: " + engine.getFuel();
    }

    public static String getText(Car car){
        return "Car: počet:" + COUNT + ", " + car.getColor();
    }
}

