package cz.cvut.od.model;

public class Engine {

    private String fuel;
    private String volume;

    public String getFuel(){
        return fuel;
    }

    public void setFuel(String fuel){
        this.fuel = fuel;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }
}
