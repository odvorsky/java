package cz.cvut.od;

import cz.cvut.od.model.Counter;
import cz.cvut.od.threads.CounterThread;
import cz.cvut.od.threads.MyFirstRunnable;
import cz.cvut.od.threads.MyFirstThread;

import java.util.Arrays;
import java.util.List;

public class ThreadPlayground {

    public void play() throws InterruptedException {
        this.counterThreads();
    }

    public void counterThreads() throws InterruptedException {
        Counter counter = new Counter();
        List<CounterThread> list = Arrays.asList(
                new CounterThread(true, counter),
                new CounterThread(true, counter),
                new CounterThread(true, counter),
                new CounterThread(true, counter),
                new CounterThread(true, counter),

                new CounterThread(false, counter),
                new CounterThread(false, counter),
                new CounterThread(false, counter),
                new CounterThread(false, counter),
                new CounterThread(false, counter));
        for(CounterThread counterThread: list){
            counterThread.start();
        }
        for(CounterThread counterThread: list){
            counterThread.join();
        }
        System.out.println("Counter is: " + counter.getCount());

    }

    public void basicThreads() {
        Thread t = new MyFirstThread();
        t.start();

        Thread t2 = new Thread(new MyFirstRunnable());
        t.start();

        System.out.println(Thread.currentThread().getName() + " called");
    }

    public void anonymousThreads() {
        final Thread t = new Thread("A") {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " executing.");
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        };
        t.start();

        final Thread t2 = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println(Thread.currentThread().getName() + " executing.");
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        }, "B");
        t2.start();

        System.out.println(Thread.currentThread().getName() + " called");
    }

    public void deamonThreads() {
        final Thread t = new Thread("A") {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " executing.");
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        };
        t.setDaemon(true);
        t.start();

        final Thread t2 = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println(Thread.currentThread().getName() + " executing.");
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        }, "B");
        t2.setDaemon(true);
        t2.start();

        System.out.println(Thread.currentThread().getName() + " called");
    }

    public void joinThreads() throws InterruptedException {
        final Thread t = new Thread("A") {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " executing.");
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        };
        t.start();

        final Thread t2 = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println(Thread.currentThread().getName() + " executing.");
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        }, "B");
        t2.start();

        t.join();
        t2.join();
        System.out.println(Thread.currentThread().getName() + " called");
    }
}
