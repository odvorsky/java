package cz.cvut.od.threads;

import cz.cvut.od.model.Counter;

public class CounterThread extends Thread{

    private boolean increment;
    private Counter counter;

    public CounterThread(boolean increment, Counter counter) {
        this.increment = increment;
        this.counter = counter;
    }

    @Override
    public void run() {
        for(int i = 0; i < 20000; i++){
            if(increment){
                counter.increment();
            }
            else {
                counter.decrement();
            }
        }

    }
}
