package cz.cvut.od.threads;

public class MyFirstThread extends Thread {

    @Override
    public void run() {
        System.out.println( Thread.currentThread().getName() + " started");
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " finished");
    }
}
