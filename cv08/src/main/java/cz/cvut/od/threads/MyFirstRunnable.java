package cz.cvut.od.threads;


public class MyFirstRunnable implements Runnable {

    public void run() {
        String name = Thread.currentThread().getName();
        System.out.println(name + " started");
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(name + " finished");
    }
}
