package cz.cvut.od.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.od.model.Game;
import cz.cvut.od.model.User;
import cz.cvut.od.model.exception.DeskSizeInvalidException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

@WebServlet(urlPatterns = "/tictac")
public class TicTacServlet extends HttpServlet {

    private ObjectMapper objectMapper = new ObjectMapper();
    private final Queue<User> queue = new PriorityQueue<>();
    private final Map<User, Game> gameMap = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String username = req.getParameter("username");
        if(username != null){
            User user = new User();
            user.setUsername(username);
            synchronized (gameMap){
                if(gameMap.containsKey(user)) {
                    resp.getWriter().println(true);
                    return;
                }
            }
        }
        resp.getWriter().println(false);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = objectMapper.readValue(req.getInputStream(), User.class);
        synchronized (queue) {
            User oponent = queue.poll();
            if (oponent != null) {
                try {
                    Game game = new Game(user, oponent, 4, 3);
                    gameMap.put(user, game);
                    gameMap.put(oponent, game);
                } catch (DeskSizeInvalidException e) {
                }
            }
            else{
                queue.add(user);
            }
        }
    }
}
