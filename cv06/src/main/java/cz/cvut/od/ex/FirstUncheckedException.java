package cz.cvut.od.ex;

public class FirstUncheckedException extends RuntimeException {

    public FirstUncheckedException() {
    }

    public FirstUncheckedException(String message) {
        super(message);
    }

    public FirstUncheckedException(String message, Throwable cause) {
        super(message, cause);
    }
}
