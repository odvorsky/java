package cz.cvut.od;

import cz.cvut.od.ex.FirstCheckedException;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static List<String> strings = new ArrayList<>();

    public static void main(String[] args) throws FirstCheckedException {
        //stackOverflow();
        //-Xmx12m do VM arguments
        /*while(false){
            strings.add(new String("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
        }*/

        new ExceptionPlayground().play();
    }


    private static void stackOverflow(){
        System.out.println("A");
        stackOverflow();
    }
}
