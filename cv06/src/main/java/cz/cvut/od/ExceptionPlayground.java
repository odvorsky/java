package cz.cvut.od;

import cz.cvut.od.ex.FirstCheckedException;
import cz.cvut.od.ex.FirstUncheckedException;

public class ExceptionPlayground {

    public void play(){
        try {
            checkedExceptionMethod();
        } catch (FirstCheckedException | IllegalStateException e) {
            throw new FirstUncheckedException("NECO SE POKAZILO", e);
        }

        uncheckedExceptionMethod();
    }

    public void uncheckedExceptionMethod(){
        throw new FirstUncheckedException("NECO SE POKAZILO");
    }

    public void checkedExceptionMethod() throws FirstCheckedException {
        throw new FirstCheckedException();
    }

}
