package cz.cvut.od;

import cz.cvut.od.model.Desk;
import cz.cvut.od.model.DeskField;
import cz.cvut.od.model.User;
import cz.cvut.od.model.exception.DeskInputException;
import cz.cvut.od.model.exception.DeskSizeInvalidException;
import org.junit.Assert;
import org.junit.Test;

public class DeskTest {

    @Test
    public void turn_validInputsSupplied_boardItemChecked() throws DeskSizeInvalidException, DeskInputException {
        Desk desk = new Desk(5);
        User user = new User("x", "David");
        desk.turn(0,0, user);

        DeskField field = desk.getDesk().get(0).get(0);
        Assert.assertEquals(user, field.getOwner());
        Assert.assertTrue(field.isChecked());
    }

    @Test(expected = DeskInputException.class)
    public void turn_invalidInputSupplied_throwDeskInputException() throws DeskSizeInvalidException, DeskInputException {
        Desk desk = new Desk(5);
        User user = new User("x", "David");
        desk.turn(-1,0, user);
    }

}
