package cz.cvut.od;

import cz.cvut.od.pg.SerializationPlayground;

public class Main {

    public static void main(String[] args){
        new SerializationPlayground().play();
    }
}
