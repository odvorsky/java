package cz.cvut.od.net;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(4444);
        System.out.println("SERVER STARTED AND LISTENING...");
        while (true) {
            new UserThread(serverSocket.accept()).start();
        }
    }
}
