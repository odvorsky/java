package cz.cvut.od.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String START_CHAR = ">";

    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in)) {
            Socket socket = new Socket("localhost", 4444);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            String line;
            while ((line = input.readLine()) != null) {
                System.out.println("Got from server: " + line);
                if (line.contains(":")) {
                    String[] split = line.split(":");
                    String cmd = split[0];
                    if ("REQ_USERNAME".equals(cmd)) {
                        String username = printReadCommand("Zadejte prosím uživatelské jméno hráče:", scanner);
                        output.println("USERNAME:" + username);
                    }
                    if ("REQ_MARK".equals(cmd)) {
                        String mark = printReadCommand("Zadejte prosím značku hráče:", scanner);
                        output.println("MARK:" + mark);
                    }
                }
                else{
                    output.println("CLIENT_READY");
                }
            }
        }
    }

    protected static String printReadCommand(String command, Scanner scanner) {
        System.out.println(command);
        System.out.print(START_CHAR);
        return scanner.nextLine();
    }

}
