package cz.cvut.od.net;

import cz.cvut.od.model.User;

import java.io.*;
import java.net.Socket;

public class UserThread extends Thread {

    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private User user = new User();


    public UserThread(Socket socket) {
        super("UserThread");
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("NEW USER THREAD STARTED..");
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
            output.println("SERVER_READY");
            String line;
            do {
                line = this.readLine();
                System.out.println(this.getName() + ": " + line);
                if (line.contains(":")) {
                    String[] split = line.split(":");
                    String cmd = split[0];
                    String data = split[1];
                    if ("USERNAME".equals(cmd)) {
                        this.user.setUsername(data);
                    }
                    if ("MARK".equals(cmd)) {
                        this.user.setMark(data);
                    }
                }
                if (user.getUsername() == null) {
                    output.println("REQ_USERNAME:Poskytněte uživatelské jméno");
                }
                if (user.getUsername() != null && user.getMark() == null) {
                    output.println("REQ_MARK:Poskytněte uživatelskou značku");
                }
                if (user.isComplete()) {
                    output.println("WAITING_GAME:Vyčkejte na připojení dalšího hráče");
                    //TODO
                    output.println("GAME_PAIRED:Hra napárována");
                }
            } while (!"EXIT".equals(line));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeQuietly(input, output, socket);
        }
    }

    private String readLine() {
        try {
            return input.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void closeQuietly(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            try {
                if (closeable != null) {
                    closeable.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
