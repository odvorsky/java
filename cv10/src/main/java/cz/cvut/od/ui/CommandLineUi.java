package cz.cvut.od.ui;

import cz.cvut.od.model.DeskField;
import cz.cvut.od.model.Game;
import cz.cvut.od.model.User;
import cz.cvut.od.model.exception.DeskInputException;
import cz.cvut.od.model.exception.DeskSizeInvalidException;

import java.util.List;
import java.util.Scanner;

public class CommandLineUi {

    private static final String START_CHAR = ">";
    private static final String QUIT_CMD = "quit";

    private Game game;

    public void start() {
        try (Scanner scanner = new Scanner(System.in)) {
            User one = this.initializeUser(scanner, 1);
            User two = this.initializeUser(scanner, 2);
            int deskSize = Integer.parseInt(this.printReadCommand("Zadejte velikost hrací desky: ", scanner));
            int turnsToWin = Integer.parseInt(this.printReadCommand("Zadejte počet tahů k vítězství: ", scanner));
            try {
                game = new Game(one, two, deskSize, turnsToWin);
                this.printPlayerOnTurn();
                this.printDesk();
                boolean end;
                do {
                    end = this.turn(scanner);
                } while (!end);
            } catch (DeskSizeInvalidException e) {
                System.out.println("Zadána nevalidní velikost hrací desky!");
            }
        }

    }

    protected boolean turn(Scanner scanner) {
        System.out.println("Zadejte souřadnice tahu (x,y): ");
        boolean valid = false;
        while (!valid) {
            try {
                String input = scanner.nextLine();
                if (QUIT_CMD.equals(input)) {
                    System.out.println("Piškvorky ukončeny..");
                    return true;
                }
                this.parseAndMakeTurn(input);
                valid = true;
            } catch (NumberFormatException e) {
                System.out.println("Zadaná souřadnice není číslo!");
                valid = false;
            } catch (DeskInputException e) {
                System.out.println("Souřadnice je zabraná nebo mimo rozsah!");
                valid = false;
            }
        }
        User winner = this.game.getWinner();
        if (winner != null) {
            System.out.println("Vítězí hráč: " + winner);
            return true;
        }
        this.printDesk();
        this.printPlayerOnTurn();
        return false;
    }

    protected void parseAndMakeTurn(String input) throws DeskInputException {
        String[] parsed = input.trim().split(",");
        int x = Integer.parseInt(parsed[0]);
        int y = Integer.parseInt(parsed[1]);
        this.game.turn(x, y);
    }


    protected User initializeUser(Scanner scanner, int ordinal) {
        String username = this.printReadCommand(String.format("Zadejte uživatelské jméno %s. hráče:", ordinal), scanner);
        String mark = this.printReadCommand(String.format("Zadejte značku %s. hráče:", ordinal), scanner);
        return new User(mark,username);
    }

    protected String printReadCommand(String command, Scanner scanner) {
        System.out.println(command);
        System.out.print(START_CHAR);
        return scanner.nextLine();
    }

    protected void printPlayerOnTurn() {
        System.out.println("Na tahu je hráč: " + this.game.getPlayerOnTurn());
    }

    protected void printDesk() {
        for (List<DeskField> row : this.game.getDesk().getDesk()) {
            StringBuilder builder = new StringBuilder();
            for (DeskField item : row) {
                builder.append(item).append(" ");
            }
            System.out.println(builder.toString());
        }
    }

}
