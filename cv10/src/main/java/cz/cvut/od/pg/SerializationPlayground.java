package cz.cvut.od.pg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.od.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class SerializationPlayground {

    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(SerializationPlayground.class);

    public void play() {
        fromJson();
        logging();
    }

    public void logging(){
        log.info("LOGGED");
    }

    public void fromJson(){
        String json = "{\"mark\":\"John Doe\",\"username\":\"X\", \"usernameXXX\":\"X\"}";
        try {
            User user = objectMapper.readValue(json, User.class);
            System.out.println(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void toJson(){
        User user = new User("John Doe", "X");
        try {
            System.out.println(objectMapper.writeValueAsString(user));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public User readObject() {
        try (FileInputStream fos = new FileInputStream("user.dat");
             ObjectInputStream oos = new ObjectInputStream(fos)) {
            // write object to file
            return (User) oos.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public void writeObject(){
        try (FileOutputStream fos = new FileOutputStream("user.dat");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            // create a new user object
            User user = new User("John Doe", "X");
            // write object to file
            oos.writeObject(user);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
