package cz.cvut.od.model;

import cz.cvut.od.model.exception.DeskInputException;
import cz.cvut.od.model.exception.DeskSizeInvalidException;

import java.util.ArrayList;
import java.util.List;

public class Desk {

    private static final int MAX_DESK_SIZE = 6;
    private static final int MIN_DESK_SIZE = 2;
    private int deskSize;
    private List<List<DeskField>> desk;

    public Desk(int deskSize) throws DeskSizeInvalidException {
        if(deskSize < MIN_DESK_SIZE){
            throw new DeskSizeInvalidException("Min deskSize = " + MIN_DESK_SIZE);
        }
        if(deskSize > MAX_DESK_SIZE){
            throw new DeskSizeInvalidException("Max deskSize = " + MAX_DESK_SIZE);
        }
        this.deskSize = deskSize;
        this.desk = new ArrayList<>(deskSize);
        for(int i = 0; i < deskSize; i++){
            List<DeskField> row = new ArrayList<>(deskSize);
            for(int j = 0; j < deskSize; j++){
                row.add(new DeskField());
            }
            this.desk.add(row);
        }
    }

    public void turn(int x, int y, User playerOnTurn) throws DeskInputException {
        if(x >= deskSize || x < 0){
            throw new DeskInputException("Input x is out of bounds, min: 0, max: " + (deskSize - 1));
        }
        if(y >= deskSize || y < 0){
            throw new DeskInputException("Input y is out of bounds, min: 0, max: " + (deskSize - 1));
        }
        this.desk.get(y).get(x).turn(playerOnTurn);
    }

    public List<List<DeskField>> getDesk() {
        return desk;
    }

    public void setDesk(List<List<DeskField>> desk) {
        this.desk = desk;
    }

}


