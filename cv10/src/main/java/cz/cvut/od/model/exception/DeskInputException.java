package cz.cvut.od.model.exception;

public class DeskInputException extends Exception {

    public DeskInputException() {
    }

    public DeskInputException(String message) {
        super(message);
    }
}
